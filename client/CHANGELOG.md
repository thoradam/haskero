## Release 0.2.1

 - **new feature**: automatic insertion of types signature (issue #12)

## Release 0.2.0

 - **new feature**: find usage / find all reference (issue #26)
 - update readme

## Release 0.1.36

 - refactore error handling (better messages, improved feedback, add additional debug info when installation environment is broken)
 - fix issue #17 (remove build and load when executing intero)
 - fix issue #20 (support windows paths with spaces)


## Release 0.1.33

 - add dependency to extension justusadam.language-haskell
 - fix uppercase name issue causing reload button to show up everytime
 - fix issue #16 (.ghci should be ignored)