export class InteroRange {
    constructor(public readonly startLine: number, public readonly startCol: number, public readonly endLine: number, public readonly endCol: number) { }
}